# README #

justsound modoki for Arduino Leonardo/pro micro.  

Written by catsin.  
2018/09  

##Use Library##


##Wiring##
Sharp X1 printer port - Arduino Leonardo/pro micro  
   data0  - pin2  
   data1  - pin3  
   data2  - pin4  
   data3  - pin5  
   data4  - pin6  
   data5  - pin7  
   data6  - pin8  
   data7  - pin9  
   strobe - pin14  
   busy   - pin15  
   GND    - GND

Audio amp - Arduino Leonardo/pro micro  
   Audio(PWM) - pin10  
   GND        - GND  

other
   LED    - pin13
   GND    - GND

   