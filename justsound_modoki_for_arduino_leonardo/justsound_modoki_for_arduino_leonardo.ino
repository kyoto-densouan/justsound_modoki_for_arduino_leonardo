// ジャストサウンドもどき for Arduino leonardo/pro micro
#include "AverageBuff.h"

#define D_PCM_DIFF_OVER_LIMIT   (48)
#define D_PCM_UPDATE_INTERVAL   (10)
#define D_PCM_RETRY_INTERVAL    (5)

AverageBuff av(3);
volatile int last_a = 0;
volatile int last_read_a = 0;

void  init_js() {
  // pin10:PWMの32KHz化
  TCCR1B &= B11111000;
  TCCR1B |= B00000001;  
  
  pinMode( 2, INPUT_PULLUP );   // data0
  pinMode( 3, INPUT_PULLUP );   // data1
  pinMode( 4, INPUT_PULLUP );   // data2
  pinMode( 5, INPUT_PULLUP );   // data3
  pinMode( 6, INPUT_PULLUP );   // data4
  pinMode( 7, INPUT_PULLUP );   // data5
  pinMode( 8, INPUT_PULLUP );   // data6
  pinMode( 9, INPUT_PULLUP );   // data7
  pinMode( 10, OUTPUT );        // Audio out(PWM)
  pinMode( 13, OUTPUT );        // LED
  pinMode( 14, INPUT_PULLUP );  // strobe
  pinMode( 15, OUTPUT );        // busy
}

void  proc_js() {
  int a=0;
  
  // データ取得(data 0～7)→D/Aコンバート
  if(digitalRead( 2 ) != 0)  a |= 0x01;
  if(digitalRead( 3 ) != 0)  a |= 0x02;
  if(digitalRead( 4 ) != 0)  a |= 0x04;
  if(digitalRead( 5 ) != 0)  a |= 0x08;
  if(digitalRead( 6 ) != 0)  a |= 0x10;
  if(digitalRead( 7 ) != 0)  a |= 0x20;
  if(digitalRead( 8 ) != 0)  a |= 0x40;
  if(digitalRead( 9 ) != 0)  a |= 0x80;

  // データの変化量による読み直し
  if( abs(a-last_read_a) > D_PCM_DIFF_OVER_LIMIT ){
    delayMicroseconds( D_PCM_RETRY_INTERVAL );
    a = 0;
    if(digitalRead( 2 ) != 0)  a |= 0x01;
    if(digitalRead( 3 ) != 0)  a |= 0x02;
    if(digitalRead( 4 ) != 0)  a |= 0x04;
    if(digitalRead( 5 ) != 0)  a |= 0x08;
    if(digitalRead( 6 ) != 0)  a |= 0x10;
    if(digitalRead( 7 ) != 0)  a |= 0x20;
    if(digitalRead( 8 ) != 0)  a |= 0x40;
    if(digitalRead( 9 ) != 0)  a |= 0x80;
  }
  last_read_a = a;
  
  // データの変化量の制限
  a = (int)av.update((double)a);

  // データに変化があった場合のみPWM設定を更新
  if( last_a != a ){
//    Serial.println(a);
    analogWrite( 10, a );
    digitalWrite( 13, HIGH );
    // 時間を待つ
    delayMicroseconds( D_PCM_UPDATE_INTERVAL );
  }else{
    digitalWrite( 13, LOW );
  }
  last_a = a;
}

void setup() {
  init_js();
  
  Serial.begin( 115200 );
}

void loop() {
//  while(true){
    proc_js();
//  }
}

