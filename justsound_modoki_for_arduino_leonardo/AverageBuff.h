#pragma once
#include  <Arduino.h>

#define D_DEFAULT_LENGTH  (10)
//----------------------------------------------------------------------------------------------
//  移動平均値を取得するためのクラス
class  AverageBuff
{
private:
  int  averageBuffSize = 0;  //  対象とする期間の長さ
  double *  pBuff = NULL;  //履歴データを保持するためのバッファ
  
  int     pnt;                    //バッファへの格納カウンタ

public:
  //  コンストラクタ
  AverageBuff()
  {
    setParam( D_DEFAULT_LENGTH );
  }

  AverageBuff( int length )
  {
    setParam( length );
  }

  ~AverageBuff()
  {
    if( pBuff != NULL ){
      delete[] pBuff;
    }
    pBuff = NULL;
  }

  void  setParam( int length ){
    pnt = 0;
    if( pBuff != NULL ){
      delete[] pBuff;
    }
    pBuff = new double[ length ];
    if( pBuff != NULL ){
      averageBuffSize = length;

      int  i;
      for( i=0; i<averageBuffSize; i++ ){
        pBuff[i]  = 0;
      }
    }else{
      averageBuffSize = 0;
    }
  }
  
  //  バッファ更新＋移動平均値取得（新データ）  
  double  update( double data ){
    double  average  = 0;
    if( averageBuffSize > 0 ){
      pBuff[pnt]  = data;
      pnt++;
      pnt  = pnt % averageBuffSize;
    
      int  i;
      for( i=0; i<averageBuffSize; i++ ){
        average  = average + pBuff[i];
      }
      average  = average / averageBuffSize;
    }
    return( average );
  }

  double getMin(){
    double min = 0;
    if( averageBuffSize > 0 ){
      min = pBuff[0];
      int  i;
      for( i=1; i<averageBuffSize; i++ ){
        if( min > pBuff[i] ){
          min = pBuff[i];
        }
      }
    }
    return( min );    
  }

  double getMax(){
    double max = 0;
    if( averageBuffSize > 0 ){
      max = pBuff[0];
      int  i;
      for( i=1; i<averageBuffSize; i++ ){
        if( max < pBuff[i] ){
          max = pBuff[i];
        }
      }
    }
    return( max );    
  }

  void reset( double vol = 0 )
  {
    int i;
    for( i=0; i<averageBuffSize; i++ ){
      pBuff[i] = vol;
    }
  }

};

